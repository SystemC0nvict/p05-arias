//
//  gameScene.m
//  FlappyGame
//
//  Created by Neo SX on 3/13/17.
//  Copyright © 2017 Neo SX. All rights reserved.
//

#import "gameScene.h"
#import "scrollBack.h"

static const uint32_t megaman_cat = 0x1 << 1;
static const uint32_t block_cat = 0x1 << 0;

int s = 0;

#define BACK_SCROLLING_SPEED .5
@interface gameScene()

@property BOOL contentCreated;
@property (nonatomic) NSMutableArray *col_r;
@property (nonatomic) NSMutableArray *col_l;

@end

@implementation gameScene{
    scrollBack * back;
    SKSpriteNode *megaman;
    SKSpriteNode *floor;
    //SKSpriteNode *floors[10];
    //SKSpriteNode *col_r;
    //CGPoint mvel;
    CGPoint bvels[10];
    SKTexture *walk1;
    SKTexture *walk2;
    SKAction *walkr;
    int p;
    int e;
    
}



- (void)didMoveToView:(SKView *)view{
    if(!self.contentCreated)
    {
        //[self makeBack];
        p = 0;
        e = 10;
        walk1 = [SKTexture textureWithImageNamed:@"walk1"];
        walk2 = [SKTexture textureWithImageNamed:@"walk2"];
        walkr = [SKAction repeatActionForever:[SKAction animateWithTextures:@[walk1,walk2] timePerFrame:.5 resize:YES restore:NO]];
        
        self.physicsWorld.contactDelegate = self;
        self.physicsWorld.gravity = CGVectorMake(0, -4);
        [self createSceneContents];
        self.contentCreated = YES;
        
    }
}

-(void)createSceneContents
{
   // self.backgroundColor = [SKColor colorWithPatternImage: [UIImage imageNamed:@"mx1intro"]];
    [self makeBack];
    self.scaleMode = SKSceneScaleModeAspectFit;
    self.physicsBody = [SKPhysicsBody bodyWithEdgeLoopFromRect:self.frame];
    [self addChild:[self megamanobj]];
    //for(int i = 0; i < e; i++){
    [self addChild:[self floormake]];
    [self make_lcols];
    [self make_rcols];
    //}
}

-(SKSpriteNode *)megamanobj
{
    //mvel.x = 0;
   // mvel.y = -20;
    megaman = [SKSpriteNode spriteNodeWithImageNamed:@"spawn"];
    
    megaman.physicsBody = [SKPhysicsBody bodyWithTexture:megaman.texture size:megaman.size];
    megaman.physicsBody = [SKPhysicsBody bodyWithRectangleOfSize:megaman.size];
    megaman.position = CGPointMake(120, 650);
    //megaman.physicsBody.velocity = CGVectorMake(0, -5);
    megaman.physicsBody.affectedByGravity = YES;
    megaman.physicsBody.dynamic = YES;
    megaman.physicsBody.allowsRotation = false;
    //[self addChild:megaman];
    megaman.physicsBody.categoryBitMask = megaman_cat;
    megaman.physicsBody.collisionBitMask = block_cat;
    return megaman;
}

-(SKSpriteNode *)floormake
{
   // if(bvels[p-1].x > self.frame.size.width)
    floor = [SKSpriteNode spriteNodeWithImageNamed:@"block"];
    floor.physicsBody = [SKPhysicsBody bodyWithTexture:floor.texture size:floor.size];
  //  floor.physicsBody = [SKPhysicsBody bodyWithRectangleOfSize:floor.texture.size];
    int randy = arc4random_uniform(300)+50;
    int randx = arc4random_uniform(bvels[p-1].x) + 400;
    if(p == 0){
        randy = 10;
        randx = 120;
    }
    floor.position = CGPointMake(randx, randy);
    
    floor.physicsBody.categoryBitMask = block_cat;
    floor.physicsBody.collisionBitMask = megaman_cat;
    floor.physicsBody.affectedByGravity = FALSE;
    floor.physicsBody.dynamic = NO;
   // floors[p] = floor;
    
    CGPoint b = floor.position;
    bvels [p] = b;
    p++;
   /* if(bvels[p-1].x+400 > self.frame.size.width){
        p -= 1;
        e = 10;
    }*/
    return floor;
}

-(void)make_lcols{
    float heights = 40;
    for(int i =0; i < 20; i++){
        SKSpriteNode *coll = [SKSpriteNode spriteNodeWithImageNamed:@"col2"];
        coll.physicsBody = [SKPhysicsBody bodyWithTexture:coll.texture size:coll.size];
        coll.position = CGPointMake(30, heights);
        coll.physicsBody = [SKPhysicsBody bodyWithRectangleOfSize:coll.frame.size];
        coll.physicsBody.affectedByGravity = false;
        coll.physicsBody.dynamic = NO;
        [self.col_l addObject:coll];
        [self addChild:coll];
        heights += 86;
    }
}

-(void)make_rcols{
    float heights = 40;
    for(int i =0; i < 20; i++){
        SKSpriteNode *colr = [SKSpriteNode spriteNodeWithImageNamed:@"col2"];
        colr.physicsBody = [SKPhysicsBody bodyWithTexture:colr.texture size:colr.size];
        colr.position = CGPointMake(384, heights);
        colr.physicsBody = [SKPhysicsBody bodyWithRectangleOfSize:colr.frame.size];
        colr.physicsBody.affectedByGravity = false;
        colr.physicsBody.dynamic = NO;
        [self.col_r addObject:colr];
        [self addChild:colr];
        heights += 86;
    }
}

-(void)makeBack{
    back = [scrollBack scrollingNodeWithImageNamed:@"mx1intro" inContainerWidth:self.size.width];
    //[back setScrollSpeed:BACK_SCROLLING_SPEED];
   // [back setAnchorPoint:CGPointZero];
    [self addChild:back];
}

-(void)update:(NSTimeInterval)currentTime
{
    CGPoint mps = megaman.position;
    //[self didBeginContact:<#(nonnull SKPhysicsContact *)#>]
    //mps.x += mvel.x;
    //mps.y += mvel.y;
    megaman.position = mps;
    CGRect m = [megaman frame];
    
    /*for(int x = 0; x < p; x++){
        CGPoint bps = floors[x].position;
        bps.x = bvels[x].x;
    }*/
    
    
        
        //for(int i; i < p; i++){
            CGRect f = [floor frame];
           // CGRect f = [floors[i] frame];
          //  NSData *imageData = [NSData dataWithContentsOfFile:@"block"];
          //  UIImage *image = [UIImage imageWithData:imageData];
         //   CGRect f = [initWithFrame: CGRectMake(100, 50, image.size.width, image.size.height)];
        
         //[megaman setTexture:[SKTexture textureWithImageNamed:@"stand"]];
                if(CGRectIntersectsRect(m, f)){
                   // mvel.y = 0;
                   
                        [megaman runAction:[SKAction setTexture:[SKTexture textureWithImageNamed:@"stand"] resize:YES]];
                    
                    //mvel.x = 5;
                    /*for(int v = 0; v < p; v++){
                        bvels[v].x = -2;
                    }*/
               // [back setScrollSpeed:BACK_SCROLLING_SPEED];

                //[megaman setTexture:[SKTexture textureWithImageNamed:@"stand"]];
                //[megaman runAction:[SKAction animateWithTextures:@[walk1,walk2] timePerFrame:.5 resize:YES restore:NO]];
                //[megaman runAction:[SKAction setTexture:[SKTexture textureWithImageNamed:@"stand"] resize:YES]];
                }
       // }
    
    /*
     
    if(mvel.y == 0 && mvel.x == 0){
        //if(megaman.texture == walk2){
            //[megaman runAction:[SKAction setTexture:[SKTexture textureWithImageNamed:@"walk1"] resize:YES]];
        //}
        [megaman runAction:walkr];
        //[SKAction waitForDuration:.5];
       // [megaman runAction:[SKAction setTexture:[SKTexture textureWithImageNamed:@"walk2"] resize:YES]];
       // [megaman runAction:[SKAction animateWithTextures:@[walk1,walk2] timePerFrame:.5 resize:YES restore:NO]];
       // mvel.x = 5;
    }*/
    
    for(int z = 0; z <20; z++){
        CGRect blol =[[_col_l objectAtIndex:z] frame];
        CGRect blor =[[_col_r objectAtIndex:z] frame];
        if(CGRectIntersectsRect(m,blol)){
            s = 1;
        }
        else if (CGRectIntersectsRect(m, blor)){
            s = 2;
        }
        else{
            //s = 0;
        }
    }
    
    for(SKSpriteNode *cols in _col_l){
        CGRect blol =[cols frame];
        if(CGRectIntersectsRect(m,blol)){
            s = 1;
        }
    }
    
    for(SKSpriteNode *cols in _col_r){
        CGRect blor = [cols frame];
        if (CGRectIntersectsRect(m, blor)){
            s = 2;
        }
    }
    
    
   /*
    if(mvel.y > 0 && mvel.x != 0 && s==0){
        [megaman runAction:[SKAction setTexture:[SKTexture textureWithImageNamed:@"jump"] resize:YES]];
    }
    if(mvel.y < 0 && mvel.x != 0 && s==0){
        [megaman runAction:[SKAction setTexture:[SKTexture textureWithImageNamed:@"fall"] resize:YES]];
    }
    if(s == 1){
        [megaman runAction:[SKAction setTexture:[SKTexture textureWithImageNamed:@"slideleft"] resize:YES]];
        mvel.y = -2;
        mvel.x = 0;
    }
    if(s == 2){
        [megaman runAction:[SKAction setTexture:[SKTexture textureWithImageNamed:@"slideright"] resize:YES]];
        mvel.y = -2;
        mvel.x = 0;
    }
    */
}


-(void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event{
    UITouch *touch = [touches anyObject];
    CGPoint location = [touch locationInNode:self];
    //mvel.y = 10;
    
   
        //[megaman.physicsBody applyImpulse:CGVectorMake(5,15)];
        //mvel.y = 10;
        //[megaman.physicsBody applyImpulse:CGVectorMake(5, 5)];
        //[SKAction waitForDuration:3];
        //mvel.y  = 0;
    
    if(location.x > 220){
        //mvel.x = 4;
        [megaman.physicsBody applyImpulse:CGVectorMake(10, 25)];
    }
    else{
        [megaman.physicsBody applyImpulse:CGVectorMake(-10, 25)];
        //mvel.x = -4;
    }
    
}

-(void)touchesEnded:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event{
    UITouch *touch = [touches anyObject];
    CGPoint location = [touch locationInNode:self];
    
        //megaman.
        //mvel.y = -7;
    
}

-(void)didBeginContact:(SKPhysicsContact *)contact{
    SKPhysicsBody *body1 , *body2;
    if(contact.bodyA.categoryBitMask < contact.bodyB.categoryBitMask)
    {
        body1 = contact.bodyA;
        body2 = contact.bodyB;
    }
    else{
        body1 = contact.bodyB;
        body2 = contact.bodyA;
    }
    if((body1.categoryBitMask == megaman_cat)&&(body2.categoryBitMask == block_cat)){
        [megaman runAction:[SKAction animateWithTextures:@[walk1,walk2] timePerFrame:.5 resize:YES restore:NO]];
    }
}

@end
